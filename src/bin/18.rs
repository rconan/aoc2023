use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut pt1_det = 0;
    let mut pt1_circ = 0;
    let mut pt1_last = (0, 0);
    let mut pt2_det = 0;
    let mut pt2_circ = 0;
    let mut pt2_last = (0, 0);
    for line in input.lines().map(|l| l.unwrap()) {
        let mut parts = line.split(' ');

        let pt1_dir = parts.next().unwrap();
        let pt1_dist = parts.next().unwrap().parse::<i32>().unwrap();
        pt1_circ += pt1_dist;
        let pt1_next = match pt1_dir {
            "U" => (pt1_last.0, pt1_last.1 - pt1_dist),
            "D" => (pt1_last.0, pt1_last.1 + pt1_dist),
            "L" => (pt1_last.0 - pt1_dist, pt1_last.1),
            "R" => (pt1_last.0 + pt1_dist, pt1_last.1),
            c => panic!("Invalid direction {}", c),
        };
        pt1_det += pt1_last.0 * pt1_next.1;
        pt1_det -= pt1_last.1 * pt1_next.0;
        pt1_last = pt1_next;

        let pt2 = parts.next().unwrap().as_bytes();
        let mut pt2_dist = 0;
        for b in &pt2[2..7] {
            pt2_dist *= 16;
            pt2_dist += (*b as char).to_digit(16).unwrap() as i64;
        }
        pt2_circ += pt2_dist;
        let pt2_next = match pt2[7] {
            b'0' => (pt2_last.0 + pt2_dist, pt2_last.1),
            b'1' => (pt2_last.0, pt2_last.1 + pt2_dist),
            b'2' => (pt2_last.0 - pt2_dist, pt2_last.1),
            b'3' => (pt2_last.0, pt2_last.1 - pt2_dist),
            c => panic!("Invalid direction {}", c),
        };
        pt2_det += pt2_last.0 * pt2_next.1;
        pt2_det -= pt2_last.1 * pt2_next.0;
        pt2_last = pt2_next;
    }
    println!("{}", (pt1_det + pt1_circ) / 2 + 1);
    println!("{}", (pt2_det + pt2_circ) / 2 + 1);
}
