use bitvec::macros::internal::funty::Fundamental;
use bitvec::prelude::BitVec;
use num::integer::lcm;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn read_bytes(s: &str, start: usize, end: usize) -> u16 {
    s[start..end]
        .as_bytes()
        .iter()
        .fold(0, |acc, b| acc * 26 + (b - b'A') as u16)
}

fn follow_instructions(nodes: &[(u16, u16); 17576], instructions: &BitVec, start: u16) -> usize {
    let mut loc = start;
    for (i, inst) in instructions.iter().cycle().enumerate() {
        if loc % 26 == 25 {
            return i;
        }
        loc = if inst.as_bool() {
            nodes[loc as usize].1
        } else {
            nodes[loc as usize].0
        };
    }
    0
}

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    let mut r = file.read_line(&mut line).unwrap();
    debug_assert_ne!(r, 0);
    let instructions = line
        .trim_end()
        .bytes()
        .map(|b| b & 0x2 != 0x2)
        .collect::<BitVec>();
    r = file.read_line(&mut line).unwrap();
    debug_assert_ne!(r, 0);
    line.clear();
    let mut ghosts = Vec::new();
    let mut nodes = [(0u16, 0u16); 17576];
    while file.read_line(&mut line).unwrap() != 0 {
        let source = read_bytes(line.as_str(), 0, 3);
        if source % 26 == 0 {
            ghosts.push(source);
        }
        let left = read_bytes(line.as_str(), 7, 10);
        let right = read_bytes(line.as_str(), 12, 15);
        nodes[source as usize] = (left, right);
        line.clear();
    }
    let part1 = follow_instructions(&nodes, &instructions, 0);
    let part2 = ghosts
        .iter()
        .map(|&l| follow_instructions(&nodes, &instructions, l))
        .reduce(lcm)
        .unwrap();
    println!("{}\n{}", part1, part2);
}
