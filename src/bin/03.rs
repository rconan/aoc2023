use bitvec::prelude::BitVec;
use std::cmp::{max, min};
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    let mut symbols = Vec::new();
    let mut numbers = Vec::new();
    let mut gears = Vec::new();
    while file.read_line(&mut line).unwrap() != 0 {
        symbols.push(symbols_from_line(line.as_str()));
        numbers.push(numbers_from_line(line.as_str()));
        gears.push(gears_from_line(line.as_str()));
        line.clear();
    }
    let mut part1_acc = 0;
    for (lineno, numbers) in numbers.iter().enumerate() {
        for &(mask_start, mask_end, number) in numbers.iter() {
            let istart = max(0, lineno as isize - 1) as usize;
            'lineloop: for syms in symbols
                .iter()
                .take(min(symbols.len() - 1, lineno + 1) + 1)
                .skip(istart)
            {
                for j in mask_start..mask_end {
                    if syms[j] {
                        part1_acc += number;
                        break 'lineloop;
                    }
                }
            }
        }
    }
    let mut part2_acc = 0;
    for (lineno, gears) in gears.iter().enumerate() {
        for &gear in gears {
            let candidates = (max(0, lineno as isize - 1) as usize..min(numbers.len(), lineno + 2))
                .flat_map(|i| numbers.get(i).unwrap().iter());
            let relevant = relevant_numbers(gear, candidates);
            if relevant.len() == 2 {
                part2_acc += relevant[0] * relevant[1];
            }
        }
    }
    println!("{}\n{}", part1_acc, part2_acc);
}

fn relevant_numbers<'a>(
    gear_pos: usize,
    numbers: impl Iterator<Item = &'a (usize, usize, usize)>,
) -> Vec<usize> {
    let mut ret = Vec::new();
    for &(mask_start, mask_end, number) in numbers {
        if gear_pos >= mask_start && gear_pos < mask_end {
            ret.push(number);
        }
    }
    ret
}

fn numbers_from_line(line: &str) -> Vec<(usize, usize, usize)> {
    let mut acc: usize = 0;
    let mut start = 0;
    let mut ret = Vec::new();
    for (i, c) in line.chars().enumerate() {
        if c.is_ascii_digit() {
            acc *= 10;
            acc += c.to_digit(10).unwrap() as usize;
        } else {
            if i != start {
                let mask_start = max(start as isize - 1, 0) as usize;
                let mask_end = min(i + 1, line.len());
                ret.push((mask_start, mask_end, acc));
            }
            start = i + 1;
            acc = 0;
        }
    }
    ret
}

fn symbols_from_line(line: &str) -> BitVec {
    let mut ret = BitVec::new();
    for c in line.chars() {
        ret.push(!c.is_ascii_digit() && c != '.' && c != '\n');
    }
    ret
}

fn gears_from_line(line: &str) -> Vec<usize> {
    let mut ret = Vec::new();
    for (i, c) in line.chars().enumerate() {
        if c == '*' {
            ret.push(i);
        }
    }
    ret
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_process_line() {
        assert_eq!(
            numbers_from_line("467..114.."),
            vec![(0, 4, 467), (4, 9, 114)]
        );
    }
}
