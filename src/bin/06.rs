use std::fs::File;
use std::io::{BufRead, BufReader};
use std::{env, iter};

fn race_count((time, distance): (usize, usize)) -> usize {
    let half_time = time as f64 / 2f64;
    let s = (half_time * half_time - distance as f64).sqrt();
    let x1 = half_time - s;
    let x2 = half_time + s;
    (x2 - 1f64).ceil() as usize + 1 - (x1 + 1f64).floor() as usize
}

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut r;
    let mut line1 = String::new();
    r = file.read_line(&mut line1).unwrap();
    debug_assert_ne!(r, 0);
    let mut line2 = String::with_capacity(line1.len());
    r = file.read_line(&mut line2).unwrap();
    debug_assert_ne!(r, 0);
    let r = iter::zip(
        line1[11..]
            .split_ascii_whitespace()
            .map(|s| s.parse::<usize>().unwrap()),
        line2[11..]
            .split_ascii_whitespace()
            .map(|s| s.parse::<usize>().unwrap()),
    )
    .map(race_count)
    .product::<usize>();
    let time = line1[11..]
        .trim()
        .replace(' ', "")
        .parse::<usize>()
        .unwrap();
    let distance = line2[11..]
        .trim()
        .replace(' ', "")
        .parse::<usize>()
        .unwrap();
    println!("{}\n{}", r, race_count((time, distance)));
}
