use itertools::Itertools;
use num::integer::lcm;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use thiserror::Error;

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
enum Pulse {
    #[default]
    Low,
    High,
}

impl Pulse {
    fn toggle(&mut self) {
        *self = match self {
            Pulse::Low => Pulse::High,
            Pulse::High => Pulse::Low,
        }
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid module type: {0}")]
    InvalidModuleType(u8),
}

trait Module {
    fn connect(&mut self, module: u16);
    fn handle(&mut self, module: u16, p: Pulse) -> Option<Pulse>;
}

fn index(b: &[u8]) -> u16 {
    debug_assert_eq!(b.len(), 2);
    (b[0] - b'a') as u16 * 26 + (b[1] - b'a') as u16
}

fn parse_targets(targets: &str) -> Vec<u16> {
    targets
        .split(", ")
        .map(|s| index(s.as_bytes()))
        .collect::<Vec<u16>>()
}

#[derive(Default)]
struct FlipFlopModule {
    state: Pulse,
}

impl Module for FlipFlopModule {
    fn connect(&mut self, _module: u16) {}
    fn handle(&mut self, _module: u16, p: Pulse) -> Option<Pulse> {
        if p == Pulse::Low {
            self.state.toggle();
            Some(self.state)
        } else {
            None
        }
    }
}

struct ConjunctionModule {
    input_state: [Option<Pulse>; 676],
}

impl Default for ConjunctionModule {
    fn default() -> Self {
        const NONE: Option<Pulse> = None;
        Self {
            input_state: [NONE; 676],
        }
    }
}

impl Module for ConjunctionModule {
    fn connect(&mut self, module: u16) {
        self.input_state[module as usize] = Some(Pulse::Low)
    }
    fn handle(&mut self, module: u16, p: Pulse) -> Option<Pulse> {
        self.input_state[module as usize] = Some(p);
        match self
            .input_state
            .iter()
            .filter_map(|i| *i)
            .all(|p| p == Pulse::High)
        {
            true => Some(Pulse::Low),
            false => Some(Pulse::High),
        }
    }
}

#[derive(Default)]
struct BroadcastModule {}

impl Module for BroadcastModule {
    fn connect(&mut self, _module: u16) {}
    fn handle(&mut self, _module: u16, p: Pulse) -> Option<Pulse> {
        Some(p)
    }
}

type CabledModule = (Box<dyn Module>, Vec<u16>);

fn push_button(
    machine: &mut [Option<CabledModule>; 677],
    count: usize,
    targets: &[u16],
    hits: &mut [Option<usize>],
) -> (usize, usize) {
    let mut queue = VecDeque::new();
    let mut lows = 0;
    let mut highs = 0;
    queue.push_back((677u16, 676u16, Pulse::Low));
    lows += 1;
    while let Some((s, d, p)) = queue.pop_front() {
        if p == Pulse::High {
            if let Some((idx, _)) = targets.iter().find_position(|&&n| n == s) {
                hits[idx] = Some(count);
            }
        }
        if let Some((ref mut module, outputs)) = machine[d as usize].as_mut() {
            if let Some(pulse) = module.handle(s, p) {
                match pulse {
                    Pulse::Low => lows += outputs.len(),
                    Pulse::High => highs += outputs.len(),
                }
                queue.extend(outputs.iter().map(|output| (d, *output, pulse)))
            }
        }
    }
    (lows, highs)
}

fn main() {
    let input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    const NO_MODULE: Option<(Box<dyn Module>, Vec<u16>)> = None;
    let mut machine = [NO_MODULE; 677];
    let target_index = index("rx".as_bytes());
    let mut final_conjunction = None;
    for line in input.lines().map(|l| l.unwrap()) {
        let (module, targets_str) = line.split_once(" -> ").unwrap();
        let idx = index(module[1..3].as_bytes());
        let targets = parse_targets(targets_str);
        if targets.contains(&target_index) {
            final_conjunction = Some(idx);
        }
        match module.as_bytes()[0] {
            b'%' => machine[idx as usize] = Some((Box::<FlipFlopModule>::default(), targets)),
            b'&' => machine[idx as usize] = Some((Box::<ConjunctionModule>::default(), targets)),
            b'b' => machine[676] = Some((Box::<BroadcastModule>::default(), targets)),
            b => panic!("Invalid module type {}", b),
        }
    }
    debug_assert!(final_conjunction.is_some());
    let mut final_inputs = Vec::new();
    for i in 0..machine.len() {
        if machine[i].is_some() {
            let outputs = machine[i].as_ref().unwrap().1.clone();
            for o in outputs {
                if o == final_conjunction.unwrap() {
                    final_inputs.push(i as u16);
                }
                if let Some((module, _)) = machine[o as usize].as_mut() {
                    module.connect(i as u16);
                }
            }
        }
    }
    let mut lows = 0;
    let mut highs = 0;
    const NO_HIT: Option<usize> = None;
    let mut hits = [NO_HIT; 4];
    for i in 1..=1000 {
        let (l, h) = push_button(&mut machine, i, &final_inputs, &mut hits);
        lows += l;
        highs += h;
    }
    let mut i = 1001;
    while hits.iter().any(|h| h.is_none()) {
        push_button(&mut machine, i, &final_inputs, &mut hits);
        i += 1
    }
    println!(
        "{}\n{}",
        lows * highs,
        hits.iter().map(|h| h.unwrap()).reduce(lcm).unwrap()
    );
}
