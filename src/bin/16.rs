use bitvec::prelude::bitvec;
use itertools::Itertools;
use memmap2::Mmap;
use std::cmp::max;
use std::collections::VecDeque;
use std::env;
use std::fs::File;

#[derive(Clone, Copy, Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

fn simulate(
    input: &[u8],
    width: usize,
    row_length: usize,
    height: usize,
    grid_length: usize,
    start_idx: usize,
    start_dir: Direction,
) -> usize {
    let mut queue = VecDeque::new();
    queue.push_back((start_idx, start_dir));
    let mut energised = bitvec![0; row_length * height];
    let mut done = bitvec![0; row_length * height * 4];
    while let Some((idx, dir)) = queue.pop_front() {
        let state_idx = idx + grid_length * dir as usize;
        if !done[state_idx] {
            done.set(state_idx, true);
            energised.set(idx, true);
            match (input[idx], dir) {
                (b'.', Direction::Up)
                | (b'/', Direction::Right)
                | (b'\\', Direction::Left)
                | (b'|', Direction::Up) => {
                    if idx > row_length {
                        queue.push_back((idx - row_length, Direction::Up))
                    }
                }
                (b'.', Direction::Down)
                | (b'/', Direction::Left)
                | (b'\\', Direction::Right)
                | (b'|', Direction::Down) => {
                    if idx + row_length < input.len() {
                        queue.push_back((idx + row_length, Direction::Down))
                    }
                }
                (b'.', Direction::Left)
                | (b'/', Direction::Down)
                | (b'\\', Direction::Up)
                | (b'-', Direction::Left) => {
                    if idx % row_length > 0 {
                        queue.push_back((idx - 1, Direction::Left))
                    }
                }
                (b'.', Direction::Right)
                | (b'/', Direction::Up)
                | (b'\\', Direction::Down)
                | (b'-', Direction::Right) => {
                    if width - idx % row_length > 1 {
                        queue.push_back((idx + 1, Direction::Right))
                    }
                }
                (b'|', Direction::Left) | (b'|', Direction::Right) => {
                    if idx > row_length {
                        queue.push_back((idx - row_length, Direction::Up))
                    }
                    if idx + row_length < input.len() {
                        queue.push_back((idx + row_length, Direction::Down))
                    }
                }
                (b'-', Direction::Up) | (b'-', Direction::Down) => {
                    if idx % row_length > 0 {
                        queue.push_back((idx - 1, Direction::Left))
                    }
                    if width - idx % row_length > 1 {
                        queue.push_back((idx + 1, Direction::Right))
                    }
                }
                (b, _) => panic!("Invalid tile {b}"),
            }
        }
    }
    energised.count_ones()
}

fn main() {
    let input = unsafe { Mmap::map(&File::open(env::args().nth(1).unwrap()).unwrap()).unwrap() };
    let (width, _) = input.iter().find_position(|&&b| b == b'\n').unwrap();
    let row_length = width + 1;
    let height = input.len() / row_length;
    let grid_length = row_length * height;
    let part1 = simulate(
        &input,
        width,
        row_length,
        height,
        grid_length,
        0,
        Direction::Right,
    );
    let up = (grid_length - row_length..grid_length - 1)
        .map(|idx| {
            simulate(
                &input,
                width,
                row_length,
                height,
                grid_length,
                idx,
                Direction::Up,
            )
        })
        .max()
        .unwrap();
    let down = (0..width)
        .map(|idx| {
            simulate(
                &input,
                width,
                row_length,
                height,
                grid_length,
                idx,
                Direction::Down,
            )
        })
        .max()
        .unwrap();
    let left = (width - 1..grid_length)
        .step_by(row_length)
        .map(|idx| {
            simulate(
                &input,
                width,
                row_length,
                height,
                grid_length,
                idx,
                Direction::Left,
            )
        })
        .max()
        .unwrap();
    let right = max(
        part1,
        (row_length..grid_length)
            .step_by(row_length)
            .map(|idx| {
                simulate(
                    &input,
                    width,
                    row_length,
                    height,
                    grid_length,
                    idx,
                    Direction::Right,
                )
            })
            .max()
            .unwrap(),
    );
    println!(
        "{}\n{}",
        part1,
        [up, down, left, right].iter().max().unwrap()
    );
}
