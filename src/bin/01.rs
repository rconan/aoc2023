use memmap2::Mmap;
use std::env;
use std::fs::File;

fn main() {
    let mmap = unsafe { Mmap::map(&File::open(env::args().nth(1).unwrap()).unwrap()).unwrap() };
    let input = std::str::from_utf8(&mmap[..]).unwrap();
    let (part1, part2) = input.lines().fold((0, 0), |(p1, p2), l| {
        (p1 + process_line_one(l), p2 + process_line_two(l))
    });
    println!("{}\n{}", part1, part2);
}

fn process_line_one(line: &str) -> u64 {
    let mut first = '\0';
    let mut last = '\0';
    for c in line.chars() {
        if c.is_ascii_digit() {
            if first == '\0' {
                first = c;
            }
            last = c;
        }
    }
    format!("{}{}", first, last).parse().unwrap()
}

fn process_line_two(line: &str) -> u64 {
    let first = first_digit(line);
    let last = last_digit(line).unwrap();
    format!("{}", first * 10 + last).parse().unwrap()
}

fn starts_with_digit(s: &str) -> Option<u32> {
    s.chars().next().unwrap().to_digit(10).or([
        (1, "one"),
        (2, "two"),
        (3, "three"),
        (4, "four"),
        (5, "five"),
        (6, "six"),
        (7, "seven"),
        (8, "eight"),
        (9, "nine"),
    ]
    .iter()
    .find_map(|&(digit, digit_str)| {
        if s.starts_with(digit_str) {
            Some(digit)
        } else {
            None
        }
    }))
}

fn first_digit(line: &str) -> u32 {
    match starts_with_digit(line) {
        Some(d) => d,
        None => first_digit(&line[1..]),
    }
}

fn last_digit(line: &str) -> Option<u32> {
    if line.is_empty() {
        None
    } else {
        match last_digit(&line[1..]) {
            Some(d) => Some(d),
            None => starts_with_digit(line),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_process_line() {
        assert_eq!(process_line_one("1abc2"), 12);
        assert_eq!(process_line_one("pqr3stu8vwx"), 38);
        assert_eq!(process_line_one("a1b2c3d4e5f"), 15);
        assert_eq!(process_line_one("treb7uchet"), 77);
    }

    #[test]
    fn test_first_digit() {
        assert_eq!(first_digit("two1nine"), 2);
        assert_eq!(first_digit("eightwothree"), 8);
        assert_eq!(first_digit("abcone2threexyz"), 1);
        assert_eq!(first_digit("xtwone3four"), 2);
        assert_eq!(first_digit("4nineeightseven2"), 4);
        assert_eq!(first_digit("zoneight234"), 1);
        assert_eq!(first_digit("7pqrstsixteen"), 7);
    }

    #[test]
    fn test_last_digit() {
        assert_eq!(last_digit("two1nine"), Some(9));
        assert_eq!(last_digit("eightwothree"), Some(3));
        assert_eq!(last_digit("abcone2threexyz"), Some(3));
        assert_eq!(last_digit("xtwone3four"), Some(4));
        assert_eq!(last_digit("4nineeightseven2"), Some(2));
        assert_eq!(last_digit("zoneight234"), Some(4));
        assert_eq!(last_digit("7pqrstsixteen"), Some(6));
    }
}
