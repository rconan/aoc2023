use bitvec::macros::internal::funty::Fundamental;
use bitvec::prelude::BitVec;
use num::PrimInt;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::BitOr;
use tailcall::trampoline;

fn symmetry_errors<T: PrimInt>(list: &[T], n: usize) -> usize {
    #[inline(always)]
    fn symmetry_errors_inner<T: PrimInt>(
        (mut acc, list, n): (usize, &[T], usize),
    ) -> trampoline::Next<(usize, &[T], usize), usize> {
        if list.is_empty() || n == 0 {
            return trampoline::Finish(acc);
        }
        if 2 * n <= list.len() {
            acc += (list[0] ^ list[2 * n - 1]).count_ones() as usize;
        }
        trampoline::Recurse((acc, &list[1..], n - 1))
    }
    trampoline::run(symmetry_errors_inner, (0, list, n))
}

fn find_symmetry_with_errors<T: PrimInt>(list: &[T], errors: usize) -> Option<usize> {
    (1..list.len()).find(|&i| symmetry_errors(list, i) == errors)
}

fn find_grid_symmetry_with_errors(grid: &BitVec, width: usize, errors: usize) -> Option<usize> {
    let rows = grid
        .chunks(width)
        .map(|r| r.iter_ones().map(|i| 1 << i).reduce(BitOr::bitor).unwrap())
        .collect::<Vec<u32>>();
    find_symmetry_with_errors(&rows, errors)
        .map(|n| 100 * n)
        .or_else(|| {
            let columns = (0..width)
                .map(|i| {
                    grid.iter()
                        .skip(i)
                        .step_by(width)
                        .enumerate()
                        .fold(0, |acc, (i, b)| acc | b.as_u32() << i)
                })
                .collect::<Vec<u32>>();
            find_symmetry_with_errors(&columns, errors)
        })
}

fn main() {
    let file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut lines = file.lines().map(|l| l.unwrap()).peekable();
    let (mut part1, mut part2) = (0, 0);
    while let Some(line) = lines.peek() {
        let width = line.len();
        let mut grid: BitVec = BitVec::new();
        for line in lines.by_ref() {
            if line.is_empty() {
                break;
            }
            grid.extend(line.chars().map(|c| match c {
                '.' => false,
                '#' => true,
                _ => panic!("Invalid character {c}"),
            }));
        }
        part1 += find_grid_symmetry_with_errors(&grid, width, 0).unwrap();
        part2 += find_grid_symmetry_with_errors(&grid, width, 1).unwrap();
    }
    println!("{}\n{}", part1, part2);
}
