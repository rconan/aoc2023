use bitvec::prelude::bitvec;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::{BitAnd, BitXor};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
enum Pipe {
    None = 0x00,  // ','
    Start = 0x0f, // 'S'
    NS = 0x03,    // '|'
    EW = 0x0c,    // '-'
    NE = 0x05,    // 'L'
    NW = 0x09,    // 'J'
    SW = 0x0a,    // '7'
    ES = 0x06,    // 'F'
}

impl BitAnd for Pipe {
    type Output = bool;

    fn bitand(self, rhs: Self) -> Self::Output {
        self & rhs as u8 > 0
    }
}

impl BitAnd<u8> for Pipe {
    type Output = u8;

    fn bitand(self, rhs: u8) -> Self::Output {
        let mask = invert_dir(rhs);
        self as u8 & mask
    }
}

impl BitXor<u8> for Pipe {
    type Output = u8;

    fn bitxor(self, rhs: u8) -> Self::Output {
        let mask = invert_dir(rhs);
        self as u8 & !mask
    }
}

impl From<char> for Pipe {
    fn from(c: char) -> Self {
        match c {
            '.' => Self::None,
            'S' => Self::Start,
            '|' => Self::NS,
            '-' => Self::EW,
            'L' => Self::NE,
            'J' => Self::NW,
            '7' => Self::SW,
            'F' => Self::ES,
            _ => panic!("Invalid pipe {c}"),
        }
    }
}

impl From<u8> for Pipe {
    fn from(b: u8) -> Self {
        unsafe { std::mem::transmute(b) }
    }
}

fn find_connected(pipes: &Vec<Pipe>, width: usize, (i, j): (usize, usize)) -> (u8, (usize, usize)) {
    for (dir, ii, jj) in [
        (0x01, i as isize, j as isize - 1),
        (0x02, i as isize, j as isize + 1),
        (0x04, i as isize + 1, j as isize),
        (0x08, i as isize - 1, j as isize),
    ] {
        if ii >= 0 && jj >= 0 {
            let idx = jj as usize * width + ii as usize;
            if idx < pipes.len() && pipes[idx] & dir > 0 {
                return (dir, (ii as usize, jj as usize));
            }
        }
    }
    (0, (0, 0))
}

const DIRS: [(i8, i8); 9] = [
    (0, 0),
    (0, -1),
    (0, 1),
    (0, 0),
    (1, 0),
    (0, 0),
    (0, 0),
    (0, 0),
    (-1, 0),
];

fn invert_dir(dir: u8) -> u8 {
    ((dir >> 1) & 0x05) | ((dir << 1) & 0x0a)
}

fn follow(pipes: &[Pipe], width: usize, dir: u8, (i, j): (usize, usize)) -> (u8, (usize, usize)) {
    let new_dir = pipes[j * width + i] ^ dir;
    let (ii, jj) = DIRS[new_dir as usize];
    (
        new_dir,
        (
            (i as isize + ii as isize) as usize,
            (j as isize + jj as isize) as usize,
        ),
    )
}

fn main() {
    let file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut pipes = Vec::new();
    let mut start = (0, 0);
    let mut lines = file.lines().peekable();
    let width = lines.peek().unwrap().as_ref().unwrap().len();
    for (j, line) in lines.map(|l| l.unwrap()).enumerate() {
        for (i, c) in line.chars().enumerate() {
            let idx = j * width + i;
            if idx >= pipes.len() {
                pipes.resize(idx + 1, Pipe::None);
            }
            pipes[idx] = Pipe::from(c);
            if pipes[idx] == Pipe::Start {
                start = (i, j);
            }
        }
    }
    let mut active = bitvec![0; pipes.len()];
    let (init_dir, (mut i, mut j)) = find_connected(&pipes, width, start);
    let mut dir = init_dir;
    let mut dist = 1;
    while pipes[j * width + i] != Pipe::Start {
        dist += 1;
        active.set(j * width + i, true);
        (dir, (i, j)) = follow(&pipes, width, dir, (i, j));
    }
    active.set(j * width + i, true);
    pipes[j * width + i] = Pipe::from(init_dir | invert_dir(dir));

    let interior_tracker = |(mut inside, mut pipe_sections, mut count): (bool, u8, usize),
                            (idx, pipe): (usize, &Pipe)| {
        if active[idx] {
            pipe_sections ^= *pipe as u8;
            if pipe_sections & 0x03 == 0x03 {
                pipe_sections = 0;
                inside = !inside;
            }
        } else if inside {
            pipe_sections = 0;
            count += 1;
        }
        (inside, pipe_sections, count)
    };

    let (_, _, part2) = pipes
        .iter()
        .enumerate()
        .fold((false, 0, 0), interior_tracker);

    println!("{}\n{}", dist / 2, part2);
}
