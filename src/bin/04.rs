use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn numbermask_from_str(s: &str) -> u128 {
    let mut numbers = 0;
    s.split(' ')
        .filter_map(|ss| ss.parse::<usize>().ok())
        .for_each(|n| numbers |= 1 << n);
    numbers
}

fn main() {
    let file = File::open(env::args().nth(1).unwrap()).unwrap();
    let stdin = BufReader::new(file);
    let lines = stdin.lines().map(|l| l.unwrap()).collect::<Vec<String>>();
    let mut points = 0usize;
    let mut counts = vec![1; lines.len()];
    for (lineno, line) in lines.iter().enumerate() {
        let (_, numbers) = line.split_once(':').unwrap();
        let (winning_numbers_str, numbers_you_have_str) = numbers.split_once('|').unwrap();
        let winning_numbers = numbermask_from_str(winning_numbers_str);
        let numbers_you_have = numbermask_from_str(numbers_you_have_str);
        let count = (winning_numbers & numbers_you_have).count_ones();
        if count > 0 {
            points += 1 << (count - 1)
        }
        for i in 1..=count as usize {
            counts[lineno + i] += counts[lineno];
        }
    }
    println!("{}\n{}", points, counts.iter().sum::<usize>());
}
