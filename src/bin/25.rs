use petgraph::prelude::{Direction, EdgeRef, StableGraph, Undirected};
use rand::Rng;
use std::env;
use std::fmt::Debug;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn reduce_to_cut<E: Debug + Default>(
    graph: &mut StableGraph<usize, E, Undirected, usize>,
) -> (usize, usize, usize) {
    while graph.node_count() > 2 {
        let edge_idx = rand::thread_rng().gen_range(0..graph.edge_count());
        let edge = graph.edge_indices().nth(edge_idx).unwrap();
        let (src, tgt) = graph.edge_endpoints(edge).unwrap();
        graph[src] += graph[tgt];
        let to_delete = tgt;
        let mut to_add = Vec::new();
        for e in graph.edges_directed(tgt, Direction::Outgoing) {
            if e.target() != src {
                to_add.push((e.target(), src));
            }
        }
        for (s, t) in to_add.iter() {
            graph.add_edge(*s, *t, E::default());
        }
        graph.remove_node(to_delete);
    }
    let edge = graph.edge_indices().next().unwrap();
    let (src, tgt) = graph.edge_endpoints(edge).unwrap();
    (graph.edge_count(), graph[src], graph[tgt])
}

fn index(b: &[u8]) -> usize {
    (b[0] - b'a') as usize * 676 + (b[1] - b'a') as usize * 26 + (b[2] - b'a') as usize
}

fn main() {
    let input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut edges = Vec::new();
    for l in input.lines() {
        let line = l.unwrap();
        let (s, tgts) = line.split_once(": ").unwrap();
        let src = index(s.as_bytes());
        for t in tgts.split(' ') {
            let tgt = index(t.as_bytes());
            edges.push((src, tgt));
        }
    }
    let mut graph: StableGraph<usize, (), Undirected, usize> = StableGraph::from_edges(edges);
    for w in graph.node_weights_mut() {
        *w = 1;
    }
    loop {
        let mut cut_graph = graph.clone();
        let (cut_length, a_weight, b_weight) = reduce_to_cut(&mut cut_graph);
        if cut_length == 3 {
            println!("{}", a_weight * b_weight);
            break;
        }
    }
}
