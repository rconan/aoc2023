use rustc_hash::FxHashMap;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Clone, Copy, PartialEq)]
enum Position {
    Empty,
    Rounded,
    Cube,
}

fn state(grid: &[Position]) -> Vec<u16> {
    grid.iter()
        .enumerate()
        .filter_map(|(i, &v)| {
            if v == Position::Rounded {
                Some(i as u16)
            } else {
                None
            }
        })
        .collect()
}

fn tilt_north(grid: &mut Vec<Position>, width: usize) {
    for i in 0..grid.len() {
        if grid[i] == Position::Rounded {
            let mut new_i = i;
            while new_i >= width && grid[new_i - width] == Position::Empty {
                new_i -= width;
            }
            if new_i != i {
                grid[i] = Position::Empty;
                grid[new_i] = Position::Rounded;
            }
        }
    }
}

fn tilt_west(grid: &mut Vec<Position>, width: usize) {
    for i in 0..grid.len() {
        if grid[i] == Position::Rounded {
            let mut new_i = i;
            while new_i > i - i % width && grid[new_i - 1] == Position::Empty {
                new_i -= 1;
            }
            if new_i != i {
                grid[i] = Position::Empty;
                grid[new_i] = Position::Rounded;
            }
        }
    }
}

fn tilt_south(grid: &mut Vec<Position>, width: usize) {
    for i in (0..grid.len()).rev() {
        if grid[i] == Position::Rounded {
            let mut new_i = i;
            while new_i < grid.len() - width && grid[new_i + width] == Position::Empty {
                new_i += width;
            }
            if new_i != i {
                grid[i] = Position::Empty;
                grid[new_i] = Position::Rounded;
            }
        }
    }
}

fn tilt_east(grid: &mut Vec<Position>, width: usize) {
    for i in (0..grid.len()).rev() {
        if grid[i] == Position::Rounded {
            let mut new_i = i;
            while new_i + 1 < width + i - (i % width) && grid[new_i + 1] == Position::Empty {
                new_i += 1;
            }
            if new_i != i {
                grid[i] = Position::Empty;
                grid[new_i] = Position::Rounded;
            }
        }
    }
}

fn weigh(grid: &[Position], width: usize, height: usize) -> usize {
    grid.iter()
        .enumerate()
        .filter_map(|(i, &v)| {
            if v == Position::Rounded {
                Some(height - i / width)
            } else {
                None
            }
        })
        .sum()
}

fn spin_loop(
    grid: &mut Vec<Position>,
    width: usize,
    states: &mut FxHashMap<Vec<u16>, usize>,
) -> Option<(usize, usize)> {
    for i in 1.. {
        let s = state(grid);
        if let Some(j) = states.insert(s, i) {
            return Some((j, i));
        }
        tilt_north(grid, width);
        tilt_west(grid, width);
        tilt_south(grid, width);
        tilt_east(grid, width);
    }
    None
}

fn spin_n(grid: &mut Vec<Position>, width: usize, n: usize) {
    for _ in 0..n {
        tilt_north(grid, width);
        tilt_west(grid, width);
        tilt_south(grid, width);
        tilt_east(grid, width);
    }
}

fn main() {
    let file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut lines = file.lines().map(|l| l.unwrap()).peekable();
    let width = lines.peek().unwrap().len();
    let mut height = 0;
    let mut grid = Vec::new();
    for line in lines {
        height += 1;
        grid.extend(line.chars().map(|c| match c {
            '.' => Position::Empty,
            'O' => Position::Rounded,
            '#' => Position::Cube,
            _ => panic!("Invalid position: {c}"),
        }));
    }

    let mut states = FxHashMap::default();
    states.insert(state(&grid), 0);

    tilt_north(&mut grid, width);
    let part1 = weigh(&grid, width, height);
    tilt_west(&mut grid, width);
    tilt_south(&mut grid, width);
    tilt_east(&mut grid, width);

    let (loop_start, loop_end) = spin_loop(&mut grid, width, &mut states).unwrap();
    spin_n(
        &mut grid,
        width,
        (1000000000 - loop_start) % (loop_end - loop_start),
    );
    let part2 = weigh(&grid, width, height);

    println!("{}\n{}", part1, part2);
}
