use itertools::Itertools;
use rustc_hash::FxHashSet;
use std::cmp::Ordering;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::iter::once;
use std::num::ParseIntError;
use thiserror::Error;

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
enum Direction {
    X,
    Y,
    Z,
}

#[derive(Debug, Error)]
pub enum BlockParseError {
    #[error("No delimiter")]
    NoDelimiter,
    #[error("Not enough dimensions {0}")]
    NotEnoughDimensions(usize),
    #[error("Too many dimensions {0}")]
    TooManyDimensions(usize),
    #[error("Invalid coordinate {0}")]
    InvalidCoordinate(#[from] ParseIntError),
}

#[derive(Debug, Eq, PartialEq)]
struct Block {
    s: (u8, u16),
    length: u8,
    direction: Direction,
}

impl Block {
    fn parse_coords(value: &str) -> Result<(u8, u8, u16), BlockParseError> {
        let mut parts = value.split(',');
        let x = parts
            .next()
            .ok_or(BlockParseError::NotEnoughDimensions(0))?
            .parse::<u8>()?;
        let y = parts
            .next()
            .ok_or(BlockParseError::NotEnoughDimensions(0))?
            .parse::<u8>()?;
        let z = parts
            .next()
            .ok_or(BlockParseError::NotEnoughDimensions(0))?
            .parse::<u16>()?;
        Ok((x, y, z))
    }

    fn start(&self) -> (u8, u8, u16) {
        (self.s.0 % 10, self.s.0 / 10, self.s.1)
    }

    fn positions(&self) -> Box<dyn Iterator<Item = u8>> {
        let (i, j, _) = self.start();
        match self.direction {
            Direction::X => Box::new((i..i + self.length).map(move |ii| 10 * j + ii)),
            Direction::Y => Box::new((j..j + self.length).map(move |jj| 10 * jj + i)),
            Direction::Z => Box::new(once(j * 10 + i)),
        }
    }
}

impl TryFrom<&str> for Block {
    type Error = BlockParseError;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let (start_str, end_str) = value.split_once('~').ok_or(BlockParseError::NoDelimiter)?;
        let start = Self::parse_coords(start_str)?;
        let end = Self::parse_coords(end_str)?;
        debug_assert!(start.0 <= end.0);
        debug_assert!(start.1 <= end.1);
        debug_assert!(start.2 <= end.2);
        let (length, direction) = if start.0 != end.0 {
            (end.0 - start.0 + 1, Direction::X)
        } else if start.1 != end.1 {
            (end.1 - start.1 + 1, Direction::Y)
        } else {
            ((end.2 - start.2 + 1) as u8, Direction::Z)
        };
        Ok(Block {
            s: (start.1 * 10 + start.0, start.2),
            length,
            direction,
        })
    }
}

impl PartialOrd for Block {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Block {
    fn cmp(&self, other: &Self) -> Ordering {
        self.s
            .1
            .cmp(&other.s.1)
            .then_with(|| self.s.0.cmp(&other.s.0))
            .then_with(|| self.direction.cmp(&other.direction))
            .then_with(|| self.length.cmp(&other.length))
    }
}

#[allow(dead_code)]
fn falling_blocks(
    supporting: &[FxHashSet<usize>],
    supported: &[FxHashSet<usize>],
    start: usize,
) -> usize {
    let mut removed = FxHashSet::default();
    let mut queue = VecDeque::new();
    queue.push_back(start);
    while let Some(latest) = queue.pop_front() {
        removed.insert(latest);
        supporting[latest]
            .iter()
            .filter(|&&i| {
                supported[i]
                    .iter()
                    .filter(|&ii| !removed.contains(ii))
                    .count()
                    == 0
            })
            .for_each(|&i| queue.push_back(i));
    }
    removed.len() - 1
}

fn main() {
    let input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap()).lines();
    let mut blocks = Vec::new();
    for line in input {
        blocks.push(Block::try_from(line.unwrap().as_str()).unwrap())
    }
    blocks.sort();
    let mut tops = [(0, 0); 100];
    let mut supporting = vec![FxHashSet::default()];
    let mut supported = vec![FxHashSet::default()];
    for (idx, block) in blocks.iter_mut().enumerate() {
        supporting.push(FxHashSet::default());
        supported.push(FxHashSet::default());
        for (height, support_idx) in block
            .positions()
            .map(|p| tops[p as usize])
            .max_set_by(|(a, _), (b, _)| a.cmp(b))
        {
            block.s.1 = height + 1;
            supporting[support_idx].insert(idx + 1);
            supported[idx + 1].insert(support_idx);
        }
        let top = if block.direction == Direction::Z {
            block.s.1 + block.length as u16 - 1
        } else {
            block.s.1
        };
        for pos in block.positions() {
            if top > tops[pos as usize].0 {
                tops[pos as usize] = (top, idx + 1);
            }
        }
    }
    let part1 = (1..=blocks.len())
        .filter(|i| supporting[*i].iter().all(|idx| supported[*idx].len() > 1))
        .count();
    let part2 = (1..=blocks.len())
        .map(|i| falling_blocks(&supporting, &supported, i))
        .sum::<usize>();
    println!("{}\n{}", part1, part2);
}
