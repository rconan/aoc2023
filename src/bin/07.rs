use crate::Hand::{FiveOfAKind, FourOfAKind, FullHouse, HighCard, OnePair, ThreeOfAKind, TwoPair};
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
#[allow(dead_code)]
enum Card {
    CX,
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
    CT,
    CJ,
    CQ,
    CK,
    CA,
}

impl From<char> for Card {
    fn from(c: char) -> Self {
        match c {
            'A' => Card::CA,
            'K' => Card::CK,
            'Q' => Card::CQ,
            'J' => Card::CJ,
            'T' => Card::CT,
            c => unsafe { std::mem::transmute(c as u8 - b'1') },
        }
    }
}

impl Card {
    fn power(&self) -> usize
    where
        Self: Sized,
    {
        let p: u8 = unsafe { std::mem::transmute(*self) };
        p as usize
    }

    fn from_with_joker(c: char) -> Self {
        match c {
            'J' => Card::CX,
            'A' => Card::CA,
            'K' => Card::CK,
            'Q' => Card::CQ,
            'T' => Card::CT,
            c => unsafe { std::mem::transmute(c as u8 - b'1') },
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
enum Hand {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

impl Hand {
    fn power(&self) -> usize
    where
        Self: Sized,
    {
        let p: u8 = unsafe { std::mem::transmute(*self) };
        p as usize
    }
}

fn hand_type(h: [Card; 5]) -> Hand {
    let mut counts = [0u8; 14];
    for card in h.iter() {
        counts[*card as usize] += 1;
    }
    let mut it = counts.iter().enumerate();
    let (_, &joker_count) = it.next().unwrap();
    let (max_index, &max_count) = it.max_by(|&(_, a), &(_, b)| a.cmp(b)).unwrap();
    match max_count + joker_count {
        5 => FiveOfAKind,
        4 => FourOfAKind,
        3 => {
            if counts
                .iter()
                .enumerate()
                .skip(1)
                .filter(|&(i, _)| i != max_index)
                .any(|(_, &c)| c == 2)
            {
                FullHouse
            } else {
                ThreeOfAKind
            }
        }
        2 => {
            if counts
                .iter()
                .enumerate()
                .skip(1)
                .any(|(i, &c)| i != max_index && c == 2)
            {
                TwoPair
            } else {
                OnePair
            }
        }
        _ => HighCard,
    }
}

fn score_hand(h: [Card; 5]) -> usize {
    let mut score = hand_type(h).power();
    for card in h.iter() {
        score <<= 4;
        score += card.power();
    }
    score
}

fn read_hand_one(s: &str) -> [Card; 5] {
    let mut hand = [Card::C2; 5];
    for (c, h) in s.chars().zip(hand.iter_mut()) {
        *h = Card::from(c);
    }
    hand
}

fn read_hand_two(s: &str) -> [Card; 5] {
    let mut hand = [Card::C2; 5];
    for (c, h) in s.chars().zip(hand.iter_mut()) {
        *h = Card::from_with_joker(c);
    }
    hand
}

struct Outcome {
    score: usize,
    bid: usize,
}

impl PartialEq<Self> for Outcome {
    fn eq(&self, other: &Self) -> bool {
        self.score.eq(&other.score)
    }
}

impl PartialOrd<Self> for Outcome {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for Outcome {}

impl Ord for Outcome {
    fn cmp(&self, other: &Self) -> Ordering {
        other.score.cmp(&self.score)
    }
}

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    let mut bids_one = BinaryHeap::new();
    let mut bids_two = BinaryHeap::new();
    while file.read_line(&mut line).unwrap() != 0 {
        let (hand_str, bid_str) = line.split_once(' ').unwrap();
        let bid = bid_str.trim_end().parse::<usize>().unwrap();
        let hand_one = read_hand_one(hand_str);
        let score_one = score_hand(hand_one);
        bids_one.push(Outcome {
            score: score_one,
            bid,
        });
        let hand_two = read_hand_two(hand_str);
        let score_two = score_hand(hand_two);
        bids_two.push(Outcome {
            score: score_two,
            bid,
        });
        line.clear();
    }
    let (mut part1, mut part2) = (0, 0);
    let mut rank = 1;
    while let Some(bid) = bids_one.pop() {
        part1 += rank * bid.bid;
        rank += 1;
    }
    rank = 1;
    while let Some(bid) = bids_two.pop() {
        part2 += rank * bid.bid;
        rank += 1;
    }
    println!("{}\n{}", part1, part2);
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    fn test_hand_type_one() {
        assert_eq!(
            hand_type([Card::CA, Card::CA, Card::CA, Card::CA, Card::CA]),
            FiveOfAKind
        );
        assert_eq!(
            hand_type([Card::CA, Card::CA, Card::CA, Card::CA, Card::CK]),
            FourOfAKind
        );
        assert_eq!(
            hand_type([Card::CA, Card::CA, Card::CA, Card::CK, Card::CK]),
            FullHouse
        );
        assert_eq!(
            hand_type([Card::CK, Card::CK, Card::CK, Card::CA, Card::CA]),
            FullHouse
        );
        assert_eq!(
            hand_type([Card::CA, Card::CA, Card::CA, Card::CK, Card::CQ]),
            ThreeOfAKind
        );
        assert_eq!(
            hand_type([Card::CA, Card::CA, Card::CK, Card::CK, Card::CQ]),
            TwoPair
        );
        assert_eq!(
            hand_type([Card::CA, Card::CA, Card::CK, Card::CQ, Card::CJ]),
            OnePair
        );
        assert_eq!(
            hand_type([Card::CA, Card::CK, Card::CQ, Card::CJ, Card::CT]),
            HighCard
        );
    }

    #[test]
    fn test_score_hand_one() {
        assert_eq!(
            score_hand([Card::CA, Card::CA, Card::CA, Card::CA, Card::CA]),
            0x6ddddd
        );
        assert_eq!(
            score_hand([Card::CJ, Card::CA, Card::CA, Card::CA, Card::CA]),
            0x5adddd
        );
        assert_eq!(
            score_hand([Card::CJ, Card::CJ, Card::CJ, Card::CJ, Card::CJ]),
            0x6aaaaa
        );
        assert_eq!(
            score_hand([Card::CJ, Card::CA, Card::CA, Card::CK, Card::CQ]),
            0x1addcb
        );
        assert_eq!(
            score_hand([Card::CA, Card::CA, Card::CK, Card::CK, Card::CQ]),
            0x2ddccb
        );
        assert_eq!(
            score_hand([Card::CJ, Card::CA, Card::CK, Card::CQ, Card::CT]),
            0x0adcb9
        );
        assert_eq!(
            score_hand([Card::C2, Card::C3, Card::C4, Card::C5, Card::C6]),
            0x012345
        );
    }

    #[test]
    fn test_score_hand_two() {
        assert_eq!(
            score_hand([Card::CA, Card::CA, Card::CA, Card::CA, Card::CA]),
            0x6ddddd
        );
        assert_eq!(
            score_hand([Card::CX, Card::CA, Card::CA, Card::CA, Card::CA]),
            0x60dddd
        );
        assert_eq!(
            score_hand([Card::CX, Card::CX, Card::CX, Card::CX, Card::CX]),
            0x600000
        );
        assert_eq!(
            score_hand([Card::CX, Card::CA, Card::CA, Card::CK, Card::CQ]),
            0x30ddcb
        );
        assert_eq!(
            score_hand([Card::CA, Card::CA, Card::CK, Card::CK, Card::CQ]),
            0x2ddccb
        );
        assert_eq!(
            score_hand([Card::CX, Card::CA, Card::CK, Card::CQ, Card::CT]),
            0x10dcb9
        );
        assert_eq!(
            score_hand([Card::C2, Card::C3, Card::C4, Card::C5, Card::C6]),
            0x012345
        );
    }
}
