use itertools::Itertools;
use std::env;
use std::fmt::{Debug, Formatter};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::num::Wrapping;

struct Hashmap {
    boxes: [Vec<(Vec<u8>, u8)>; 256],
}

impl Debug for Hashmap {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for (i, b) in self.boxes.iter().enumerate().filter(|(_, b)| !b.is_empty()) {
            write!(f, "Box {i}:")?;
            for (lbl, flen) in b {
                write!(f, " [")?;
                for b in lbl {
                    write!(f, "{}", *b as char)?;
                }
                write!(f, " {flen}]")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Default for Hashmap {
    fn default() -> Self {
        Hashmap {
            boxes: core::array::from_fn(|_| Vec::new()),
        }
    }
}

impl Hashmap {
    fn hash(input: &[u8]) -> usize {
        input
            .iter()
            .fold(Wrapping(0u8), |acc, &b| (acc + Wrapping(b)) * Wrapping(17))
            .0 as usize
    }

    fn execute(&mut self, inst: &[u8]) {
        let last = inst.len() - 1;
        match inst[last] {
            b'-' => self.remove(&inst[0..last]),
            l => self.set(&inst[0..last - 1], l - b'0'),
        }
    }

    fn remove(&mut self, label: &[u8]) {
        let b = &mut self.boxes[Self::hash(label)];
        if let Some(idx) = b
            .iter()
            .find_position(|(l, _)| l == label)
            .map(|(idx, _)| idx)
        {
            b.remove(idx);
        }
    }

    fn set(&mut self, label: &[u8], value: u8) {
        let b = &mut self.boxes[Self::hash(label)];
        if let Some(idx) = b
            .iter()
            .find_position(|(l, _)| l == label)
            .map(|(idx, _)| idx)
        {
            b[idx].1 = value
        } else {
            b.push((label.to_vec(), value));
        }
    }

    fn power(&self) -> usize {
        self.boxes
            .iter()
            .enumerate()
            .flat_map(|(i, b)| {
                b.iter()
                    .enumerate()
                    .map(move |(j, &(_, l))| (i + 1) * (j + 1) * l as usize)
            })
            .sum()
    }
}

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut buf = Vec::new();
    let mut part1 = 0;
    let mut hashmap = Hashmap::default();
    while file.read_until(b',', &mut buf).unwrap() != 0 {
        let last_byte = buf[buf.len() - 1];
        let inst = if last_byte == b',' || last_byte == b'\n' {
            &buf[0..buf.len() - 1]
        } else {
            &buf
        };
        part1 += Hashmap::hash(inst);
        hashmap.execute(inst);
        buf.clear();
    }
    let part2 = hashmap.power();

    println!("{}\n{}", part1, part2);
}
