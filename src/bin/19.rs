use std::cmp::{max, min};
use std::fmt::Formatter;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::BitAnd;
use std::{env, fmt};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid operator: {0}")]
    InvalidOperator(u8),
    #[error("Invalid parameter: {0}")]
    InvalidParameter(u8),
    #[error("Invalid predicate: {0}")]
    InvalidPredicate(String),
    #[error("Label too long: {0}")]
    LabelTooLong(String),
    #[error("Malformed rule (no ':'): {0}")]
    MalformedRule(String),
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Target {
    Accept,
    Reject,
    Send(u16),
}

impl fmt::Display for Target {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Accept => write!(f, "A")?,
            Self::Reject => write!(f, "R")?,
            Self::Send(t) => write!(
                f,
                "{}{}{}",
                ((t / 729) as u8 + b'`') as char,
                ((t / 27 % 27) as u8 + b'`') as char,
                ((t % 27) as u8 + b'`') as char
            )?,
        };
        Ok(())
    }
}

impl Target {
    fn parse(s: &str) -> Result<u16, Error> {
        if s.len() > 3 {
            return Err(Error::LabelTooLong(s.to_string()));
        }
        let mut acc = 0;
        for c in s.chars() {
            acc *= 27;
            acc += (c as u8 - b'`') as u16;
        }
        Ok(acc)
    }
}

impl TryFrom<&str> for Target {
    type Error = Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Ok(match s {
            "A" => Self::Accept,
            "R" => Self::Reject,
            _ => Self::Send(Self::parse(s)?),
        })
    }
}

#[derive(Debug)]
struct Part {
    x: u16,
    m: u16,
    a: u16,
    s: u16,
}

impl Part {
    fn rating(&self) -> u16 {
        self.x + self.m + self.a + self.s
    }
}

impl TryFrom<&str> for Part {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let numbers = value
            .split(',')
            .map(|s| s[2..].parse::<u16>().unwrap())
            .collect::<Vec<u16>>();
        debug_assert_eq!(numbers.len(), 4);
        Ok(Self {
            x: numbers[0],
            m: numbers[1],
            a: numbers[2],
            s: numbers[3],
        })
    }
}

#[derive(Clone, Copy, Debug)]
enum Param {
    X,
    M,
    A,
    S,
}

impl TryFrom<u8> for Param {
    type Error = Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'x' => Ok(Self::X),
            b'm' => Ok(Self::M),
            b'a' => Ok(Self::A),
            b's' => Ok(Self::S),
            _ => Err(Error::InvalidParameter(value)),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Op {
    LT,
    GT,
}

impl TryFrom<u8> for Op {
    type Error = Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            b'<' => Ok(Self::LT),
            b'>' => Ok(Self::GT),
            _ => Err(Error::InvalidOperator(value)),
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct Rule {
    param: Param,
    op: Op,
    threshold: u16,
    target: Target,
}

impl Rule {
    fn matches(&self, part: &Part) -> bool {
        let value = match self.param {
            Param::X => part.x,
            Param::M => part.m,
            Param::A => part.a,
            Param::S => part.s,
        };
        match self.op {
            Op::LT => value < self.threshold,
            Op::GT => value > self.threshold,
        }
    }
}

impl TryFrom<&str> for Rule {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if let Some((predicate_str, target_str)) = value.split_once(':') {
            let (lhs, rhs) = predicate_str.split_at(2);
            let param = Param::try_from(lhs.as_bytes()[0])?;
            let op = Op::try_from(lhs.as_bytes()[1])?;
            let threshold = rhs.parse::<u16>().unwrap();
            Ok(Self {
                param,
                op,
                threshold,
                target: Target::try_from(target_str).unwrap(),
            })
        } else {
            Err(Error::MalformedRule(value.to_string()))
        }
    }
}

#[derive(Debug)]
struct Workflow {
    rules: Vec<Rule>,
    fallback: Target,
}

impl Workflow {
    fn apply(&self, part: &Part) -> Target {
        for rule in self.rules.iter() {
            if rule.matches(part) {
                return rule.target;
            }
        }
        self.fallback
    }

    fn accepts(&self, workflows: &[Option<Workflow>], mut c: Constraint) -> usize {
        let mut acc = 0;
        for rule in self.rules.iter() {
            let rc = Constraint::from(rule);
            acc += match rule.target {
                Target::Accept => (c & rc).count(),
                Target::Reject => 0,
                Target::Send(t) => workflows[t as usize]
                    .as_ref()
                    .unwrap()
                    .accepts(workflows, c & rc),
            };
            c.negate(rule);
        }
        acc + match self.fallback {
            Target::Accept => c.count(),
            Target::Reject => 0,
            Target::Send(t) => workflows[t as usize]
                .as_ref()
                .unwrap()
                .accepts(workflows, c),
        }
    }
}

impl TryFrom<&str> for Workflow {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if let Some((steps, fallback_step)) = value.rsplit_once(',') {
            Ok(Self {
                rules: steps
                    .split(',')
                    .map(|s| Rule::try_from(s).unwrap())
                    .collect(),
                fallback: Target::try_from(fallback_step).unwrap(),
            })
        } else {
            Ok(Self {
                rules: vec![],
                fallback: Target::try_from(value).unwrap(),
            })
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Constraint {
    x: (u16, u16),
    m: (u16, u16),
    a: (u16, u16),
    s: (u16, u16),
}

impl Constraint {
    fn affected(&mut self, p: Param) -> &mut (u16, u16) {
        match p {
            Param::X => &mut self.x,
            Param::M => &mut self.m,
            Param::A => &mut self.a,
            Param::S => &mut self.s,
        }
    }

    fn count(&self) -> usize {
        [&self.x, &self.m, &self.a, &self.s]
            .iter()
            .map(|&(l, h)| (h - l) as usize)
            .product::<usize>()
    }

    fn symmetric_difference_range(a: (u16, u16), b: (u16, u16)) -> (u16, u16) {
        (max(a.0, b.0), min(a.1, b.1))
    }

    fn negate(&mut self, rhs: &Rule) {
        let affected = self.affected(rhs.param);
        match rhs.op {
            Op::LT => affected.0 = max(affected.0, rhs.threshold),
            Op::GT => affected.1 = min(affected.1, rhs.threshold + 1),
        }
    }
}

impl From<&Rule> for Constraint {
    fn from(value: &Rule) -> Self {
        let mut c = Constraint::default();
        let affected = c.affected(value.param);
        match value.op {
            Op::LT => affected.1 = value.threshold,
            Op::GT => affected.0 = value.threshold + 1,
        }
        c
    }
}

impl BitAnd<Constraint> for Constraint {
    type Output = Constraint;

    fn bitand(self, rhs: Constraint) -> Self::Output {
        Self::Output {
            x: Constraint::symmetric_difference_range(self.x, rhs.x),
            m: Constraint::symmetric_difference_range(self.m, rhs.m),
            a: Constraint::symmetric_difference_range(self.a, rhs.a),
            s: Constraint::symmetric_difference_range(self.s, rhs.s),
        }
    }
}

impl Default for Constraint {
    fn default() -> Self {
        Self {
            x: (1, 4001),
            m: (1, 4001),
            a: (1, 4001),
            s: (1, 4001),
        }
    }
}

fn main() {
    let mut input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    const NONE: Option<Workflow> = None;
    let mut workflows = [NONE; 19683];
    while input.read_line(&mut line).unwrap() != 0 {
        if line == "\n" {
            line.clear();
            break;
        }
        let (name, remainder) = line.split_once('{').unwrap();
        let idx = Target::parse(name).unwrap();
        let workflow = Workflow::try_from(&remainder[..remainder.len() - 2]).unwrap();
        workflows[idx as usize] = Some(workflow);
        line.clear();
    }
    let start = Target::parse("in").unwrap();
    let mut part1 = 0usize;
    while input.read_line(&mut line).unwrap() != 0 {
        let part = Part::try_from(&line[1..line.len() - 2]).unwrap();
        let mut current = start;
        loop {
            match workflows[current as usize].as_ref().unwrap().apply(&part) {
                Target::Accept => {
                    part1 += part.rating() as usize;
                    break;
                }
                Target::Reject => {
                    break;
                }
                Target::Send(t) => current = t,
            }
        }
        line.clear();
    }
    let part2 = workflows[start as usize]
        .as_ref()
        .unwrap()
        .accepts(&workflows, Constraint::default());
    println!("{part1}\n{part2}");
}
