use itertools::Itertools;
use std::cmp::{max, min};
use std::collections::BTreeMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::Bound::{Excluded, Unbounded};
use std::{env, mem};

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    let mut readlen = file.read_line(&mut line).unwrap();
    debug_assert_ne!(readlen, 0);
    let mut seeds = line
        .split_ascii_whitespace()
        .skip(1)
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    let mut seed_ranges = seeds
        .iter()
        .copied()
        .tuples()
        .collect::<Vec<(usize, usize)>>();
    let mut new_ranges = Vec::new();
    line.clear();
    let mut map: BTreeMap<usize, (usize, usize)> = BTreeMap::new();
    while !file.fill_buf().unwrap().is_empty() {
        while file.read_line(&mut line).unwrap() != 0 {
            if line == "\n" {
                readlen = file.read_line(&mut line).unwrap();
                debug_assert_ne!(readlen, 0);
                line.clear();
                break;
            }
            let (destination, source, length) = line
                .split_ascii_whitespace()
                .map(|s| s.parse::<usize>().unwrap())
                .collect_tuple()
                .unwrap();
            map.insert(source + length, (destination, length));
            line.clear();
        }
        for seed in seeds.iter_mut() {
            if let Some((source_end, (destination_start, length))) =
                map.range((Excluded(*seed), Unbounded)).next()
            {
                let source_start = source_end - length;
                if source_start <= *seed && *seed < *source_end {
                    *seed = *destination_start + *seed - source_start;
                }
            };
        }
        for (seed_start, seed_length) in seed_ranges.iter_mut() {
            for (source_end, (destination_start, length)) in
                map.range((Excluded(*seed_start), Unbounded))
            {
                let source_start = *source_end - *length;
                if source_start > *seed_start + *seed_length {
                    break;
                }
                while *seed_start < *source_end && *seed_length > 0 {
                    if source_start > *seed_start {
                        new_ranges.push((*seed_start, source_start - *seed_start));
                    }
                    let to_map_start = max(source_start, *seed_start);
                    let to_map_end = min(*source_end, *seed_start + *seed_length);
                    new_ranges.push((
                        (to_map_start as isize - source_start as isize
                            + *destination_start as isize) as usize,
                        to_map_end - to_map_start,
                    ));
                    *seed_length = max(
                        (*seed_length + *seed_start) as isize - *source_end as isize,
                        0,
                    ) as usize;
                    *seed_start = *source_end;
                }
            }
            if *seed_length > 0 {
                new_ranges.push((*seed_start, *seed_length));
            }
        }
        mem::swap(&mut seed_ranges, &mut new_ranges);
        new_ranges.clear();
        map.clear();
    }
    println!(
        "{}\n{}",
        seeds.iter().min().unwrap(),
        seed_ranges.iter().map(|(a, _)| a).min().unwrap()
    );
}
