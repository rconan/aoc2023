use petgraph::prelude::{EdgeRef, Graph, NodeIndex};
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Clone, Copy, Eq, PartialEq)]
struct State {
    f_score: u16,
    node: NodeIndex,
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .f_score
            .cmp(&self.f_score)
            .then_with(|| self.node.cmp(&other.node))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
enum Side {
    Left,
    Right,
    Top,
    Bottom,
}

fn reconstruct_path(came_from: Vec<(u32, u8)>, mut current: u32) -> u16 {
    let mut acc = 0;
    while current / 4 != 0 {
        let (new, weight) = came_from[current as usize];
        acc += weight as u16;
        current = new;
    }
    acc
}

fn find_path(width: usize, graph: &Graph<(u8, u8, Side), u8>) {
    let goal = (
        (width - 1) as u8,
        ((graph.node_count() / 4) / width - 1) as u8,
    );
    let h = |i: u8, j: u8| (goal.0 - i) as u16 + (goal.1 - j) as u16;

    let mut open_set = BinaryHeap::new();
    open_set.push(State {
        f_score: h(0, 0),
        node: NodeIndex::new(0),
    });
    open_set.push(State {
        f_score: h(0, 0),
        node: NodeIndex::new(2),
    });
    let mut came_from = vec![(0, 0); graph.node_count()];
    let mut g_score = vec![u16::MAX; graph.node_count()];
    g_score[0] = 0;
    g_score[2] = 0;

    while let Some(state) = open_set.pop() {
        let current = graph[state.node];
        if (current.0, current.1) == goal {
            println!("{}", reconstruct_path(came_from, state.node.index() as u32));
            break;
        }

        for edge in graph.edges(state.node) {
            let tentative_g_score = g_score[state.node.index()] + *edge.weight() as u16;
            let (neighbour_i, neighbour_j, _) = graph[edge.target()];
            let neighbour_index = edge.target().index();
            if tentative_g_score < g_score[neighbour_index] {
                came_from[neighbour_index] = (state.node.index() as u32, *edge.weight());
                g_score[neighbour_index] = tentative_g_score;
                let f_score = tentative_g_score + h(neighbour_i, neighbour_j);
                open_set.push(State {
                    f_score,
                    node: edge.target(),
                });
            }
        }
    }
}

fn main() {
    let mut input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap())
        .lines()
        .peekable();
    let width = input.peek().unwrap().as_ref().unwrap().len();
    let mut weights = Vec::new();
    let mut pt1_graph = Graph::new();
    let mut pt2_graph = Graph::new();
    for (j, line) in input.enumerate() {
        for (i, weight_char) in line.unwrap().chars().enumerate() {
            let weight = weight_char.to_digit(10).unwrap() as u8;
            weights.push(weight);

            let pt1_left_node = pt1_graph.add_node((i as u8, j as u8, Side::Left));
            let pt1_right_node = pt1_graph.add_node((i as u8, j as u8, Side::Right));
            let pt1_top_node = pt1_graph.add_node((i as u8, j as u8, Side::Top));
            let pt1_bottom_node = pt1_graph.add_node((i as u8, j as u8, Side::Bottom));

            let pt2_left_node = pt2_graph.add_node((i as u8, j as u8, Side::Left));
            let pt2_right_node = pt2_graph.add_node((i as u8, j as u8, Side::Right));
            let pt2_top_node = pt2_graph.add_node((i as u8, j as u8, Side::Top));
            let pt2_bottom_node = pt2_graph.add_node((i as u8, j as u8, Side::Bottom));

            let mut i_in_weight = weight;
            let mut i_out_weight = 0;
            for di in 1..=10 {
                if di > i {
                    break;
                }
                let ii = i - di;
                i_out_weight += weights[j * width + ii];
                if di <= 3 {
                    pt1_graph.add_edge(
                        NodeIndex::new((j * width + ii) * 4 + 2),
                        pt1_left_node,
                        i_in_weight,
                    );
                    pt1_graph.add_edge(
                        NodeIndex::new((j * width + ii) * 4 + 3),
                        pt1_left_node,
                        i_in_weight,
                    );
                    pt1_graph.add_edge(
                        pt1_top_node,
                        NodeIndex::new((j * width + ii) * 4 + 1),
                        i_out_weight,
                    );
                    pt1_graph.add_edge(
                        pt1_bottom_node,
                        NodeIndex::new((j * width + ii) * 4 + 1),
                        i_out_weight,
                    );
                } else if di >= 4 {
                    pt2_graph.add_edge(
                        NodeIndex::new((j * width + ii) * 4 + 2),
                        pt2_left_node,
                        i_in_weight,
                    );
                    pt2_graph.add_edge(
                        NodeIndex::new((j * width + ii) * 4 + 3),
                        pt2_left_node,
                        i_in_weight,
                    );
                    pt2_graph.add_edge(
                        pt2_top_node,
                        NodeIndex::new((j * width + ii) * 4 + 1),
                        i_out_weight,
                    );
                    pt2_graph.add_edge(
                        pt2_bottom_node,
                        NodeIndex::new((j * width + ii) * 4 + 1),
                        i_out_weight,
                    );
                }
                i_in_weight += weights[j * width + ii];
            }

            let mut j_in_weight = weight;
            let mut j_out_weight = 0;
            for dj in 1..=10 {
                if dj > j {
                    break;
                }
                let jj = j - dj;
                j_out_weight += weights[jj * width + i];
                if dj <= 3 {
                    pt1_graph.add_edge(
                        NodeIndex::new((jj * width + i) * 4),
                        pt1_top_node,
                        j_in_weight,
                    );
                    pt1_graph.add_edge(
                        NodeIndex::new((jj * width + i) * 4 + 1),
                        pt1_top_node,
                        j_in_weight,
                    );
                    pt1_graph.add_edge(
                        pt1_left_node,
                        NodeIndex::new((jj * width + i) * 4 + 3),
                        j_out_weight,
                    );
                    pt1_graph.add_edge(
                        pt1_right_node,
                        NodeIndex::new((jj * width + i) * 4 + 3),
                        j_out_weight,
                    );
                } else if dj >= 4 {
                    pt2_graph.add_edge(
                        NodeIndex::new((jj * width + i) * 4),
                        pt2_top_node,
                        j_in_weight,
                    );
                    pt2_graph.add_edge(
                        NodeIndex::new((jj * width + i) * 4 + 1),
                        pt2_top_node,
                        j_in_weight,
                    );
                    pt2_graph.add_edge(
                        pt2_left_node,
                        NodeIndex::new((jj * width + i) * 4 + 3),
                        j_out_weight,
                    );
                    pt2_graph.add_edge(
                        pt2_right_node,
                        NodeIndex::new((jj * width + i) * 4 + 3),
                        j_out_weight,
                    );
                }
                j_in_weight += weights[jj * width + i];
            }
        }
    }
    find_path(width, &pt1_graph);
    find_path(width, &pt2_graph);
}
