use bitvec::prelude::{bitvec, BitSlice, Lsb0};
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[allow(dead_code)]
fn output(rocks: &BitSlice, at: &BitSlice, width: usize) {
    for (i, (rock, at)) in rocks.iter().zip(at.iter()).enumerate() {
        if i % width == 0 {
            println!();
        }
        if *rock {
            print!("#")
        } else if *at {
            print!("O")
        } else {
            print!(".")
        }
    }
    println!();
}

fn step(rocks: &BitSlice, width: usize, from: &BitSlice, next: &mut BitSlice) {
    next.fill(false);
    for i in from.iter_ones() {
        if i > width && !rocks[i - width] {
            next.set(i - width, true);
        }
        if i + width < rocks.len() && !rocks[i + width] {
            next.set(i + width, true);
        }
        if i % width > 0 && !rocks[i - 1] {
            next.set(i - 1, true);
        }
        if width - i % width > 1 && !rocks[i + 1] {
            next.set(i + 1, true);
        }
    }
}

fn count_grids(at: &BitSlice, width: usize) -> [usize; 25] {
    let mut counts = [0; 25];
    let fifth = width / 5;
    for idx in at.iter_ones() {
        let i = (idx % width) / fifth;
        let j = (idx / width) / fifth;
        counts[j * 5 + i] += 1;
    }
    counts
}

fn main() {
    let mut input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap())
        .lines()
        .peekable();
    let width = input.peek().unwrap().as_ref().unwrap().len();
    let big_width = input.peek().unwrap().as_ref().unwrap().len() * 5;
    let mut rocks = bitvec![];
    let mut big_rocks = bitvec![0; big_width * big_width];
    let mut start = None;
    for (j, line) in input.enumerate() {
        for (i, c) in line.unwrap().chars().enumerate() {
            let b = match c {
                '#' => true,
                '.' => false,
                'S' => false,
                _ => panic!("Invalid char {c}"),
            };
            rocks.push(b);
            if b {
                for jk in 0..5 {
                    let jj = j + jk * width;
                    for ik in 0..5 {
                        let ii = i + ik * width;
                        big_rocks.set(jj * big_width + ii, true);
                    }
                }
            }
            if c == 'S' {
                start = Some((i, j));
            }
        }
    }
    let mut from = bitvec![0; rocks.len()];
    let (st_i, st_j) = start.unwrap();
    from.set(st_j * width + st_i, true);
    let part2_steps = 26501365;
    let mut big_from = bitvec![0; big_rocks.len()];
    big_from.set((st_j + 2 * width) * big_width + st_i + 2 * width, true);
    let mut big_next = bitvec![0; big_from.len()];
    let count = 2 * width + part2_steps % width;
    for _ in 0..64 {
        step(&big_rocks, big_width, &big_from, &mut big_next);
        std::mem::swap(&mut big_from, &mut big_next);
    }
    println!("{}", big_from.count_ones());
    for _ in 64..count {
        step(&big_rocks, big_width, &big_from, &mut big_next);
        std::mem::swap(&mut big_from, &mut big_next);
    }
    let sections = count_grids(&big_from, big_width);
    let n = part2_steps / width;
    let even = n & !1;
    let n_even_grids = even * even;
    let odd = if n & 1 == 0 { n - 1 } else { n };
    let n_odd_grids = odd * odd;
    let n_small_edges = n;
    let n_large_edges = n - 1;
    let even_grids = sections[13] * n_even_grids;
    let odd_grids = sections[12] * n_odd_grids;
    let small_edges = [1, 9, 15, 23]
        .iter()
        .map(|k| sections[*k] * n_small_edges)
        .sum::<usize>();
    let large_edges = [6, 8, 16, 18]
        .iter()
        .map(|k| sections[*k] * n_large_edges)
        .sum::<usize>();
    let corners = [2, 10, 14, 22].iter().map(|k| sections[*k]).sum::<usize>();
    println!(
        "{}",
        even_grids + odd_grids + small_edges + large_edges + corners
    );
}
