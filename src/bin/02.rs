use memmap2::Mmap;
use std::env;
use std::fs::File;

fn main() {
    let mmap = unsafe { Mmap::map(&File::open(env::args().nth(1).unwrap()).unwrap()).unwrap() };
    let input = std::str::from_utf8(&mmap[..]).unwrap();
    let mut sum = 0;
    let mut power_sum = 0;
    input.lines().for_each(|line| {
        let (game_part, draws_part) = line.split_once(':').unwrap();
        let (_, id_bit) = game_part.split_at(5);
        let id = id_bit.parse::<usize>().unwrap();

        let (mut max_red, mut max_green, mut max_blue) = (0, 0, 0);
        let mut possible = true;

        for draw in draws_part.split(';') {
            for colour_draw_str in draw.split(',') {
                let mut colour_draw_parts = colour_draw_str.split_ascii_whitespace();
                let count = colour_draw_parts.next().unwrap().parse().unwrap();
                match colour_draw_parts.next().unwrap() {
                    "red" => {
                        if count > 12 {
                            possible = false;
                        }
                        if count > max_red {
                            max_red = count;
                        }
                    }
                    "green" => {
                        if count > 13 {
                            possible = false;
                        }
                        if count > max_green {
                            max_green = count;
                        }
                    }
                    "blue" => {
                        if count > 14 {
                            possible = false;
                        }
                        if count > max_blue {
                            max_blue = count;
                        }
                    }
                    s => panic!("Invalid colour: '{}'", s),
                }
            }
        }
        if possible {
            sum += id;
        }
        power_sum += max_red * max_green * max_blue;
    });
    println!("{}\n{}", sum, power_sum);
}
