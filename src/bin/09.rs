use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use tailcall::trampoline;

fn new_numbers(numbers: Vec<isize>) -> (isize, isize) {
    type InnerParams = (isize, isize, isize, Vec<isize>, Vec<isize>);
    #[inline(always)]
    fn new_numbers_inner(
        (f_sign, f, l, mut numbers, mut next): InnerParams,
    ) -> trampoline::Next<InnerParams, (isize, isize)> {
        let mut non_zero = false;
        let mut iter = numbers.iter();
        let first = *iter.next().unwrap();
        let mut last = first;
        for n in iter {
            let diff = n - last;
            non_zero |= diff != 0;
            next.push(diff);
            last = *n;
        }
        if !non_zero {
            return trampoline::Finish((f + f_sign * first, l + last));
        }
        numbers.clear();
        trampoline::Recurse((-f_sign, f + f_sign * first, l + last, next, numbers))
    }
    let next = Vec::with_capacity(numbers.len());
    trampoline::run(new_numbers_inner, (1, 0, 0, numbers, next))
}

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    let (mut part1, mut part2) = (0, 0);
    while file.read_line(&mut line).unwrap() != 0 {
        let numbers = line
            .split_ascii_whitespace()
            .map(|s| s.parse::<isize>().unwrap())
            .collect::<Vec<isize>>();
        let (f, l) = new_numbers(numbers);
        part1 += l;
        part2 += f;
        line.clear();
    }
    println!("{}\n{}", part1, part2);
}
