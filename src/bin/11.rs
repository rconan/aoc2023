use bitvec::prelude::{bitvec, BitVec};
use itertools::Itertools;
use memmap2::Mmap;
use std::env;
use std::fs::File;

fn find_mesh_distance(galaxies: Vec<(usize, usize)>) -> usize {
    let mut acc = 0;
    for a in 0..galaxies.len() {
        for b in a + 1..galaxies.len() {
            acc += (galaxies[a].0 as isize - galaxies[b].0 as isize).unsigned_abs();
            acc += (galaxies[a].1 as isize - galaxies[b].1 as isize).unsigned_abs();
        }
    }
    acc
}

fn main() {
    let mmap = unsafe { Mmap::map(&File::open(env::args().nth(1).unwrap()).unwrap()).unwrap() };
    let (width, _) = mmap.iter().find_position(|&&b| b == b'\n').unwrap();
    let mut columns = bitvec![0; width];
    let mut rows: BitVec = BitVec::new();
    let mut part1_galaxies = Vec::new();
    for (j, chunk) in mmap.chunks(width + 1).enumerate() {
        rows.push(
            chunk[0..width]
                .iter()
                .enumerate()
                .filter(|&(i, &b)| {
                    if b == b'#' {
                        columns.set(i, true);
                        part1_galaxies.push((i, j));
                        true
                    } else {
                        false
                    }
                })
                .count()
                != 0,
        );
    }
    let mut part2_galaxies = Vec::with_capacity(part1_galaxies.len());
    part1_galaxies.iter_mut().for_each(|(i, j)| {
        let empty_columns = columns[0..*i].count_zeros();
        let empty_rows = rows[0..*j].count_zeros();
        part2_galaxies.push((*i + 999999 * empty_columns, *j + 999999 * empty_rows));
        *i += empty_columns;
        *j += empty_rows;
    });
    let part1 = find_mesh_distance(part1_galaxies);
    let part2 = find_mesh_distance(part2_galaxies);
    println!("{}\n{}", part1, part2);
}
