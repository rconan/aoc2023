use itertools::{repeat_n, Itertools};
use rustc_hash::FxHashMap;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use string_join::Join;

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
#[repr(u8)]
enum Spring {
    Oper = b'.',
    Dmgd = b'#',
    Unkn = b'?',
}

fn possibilities(
    cache: &mut FxHashMap<(Vec<Spring>, Vec<usize>), usize>,
    springs: &Vec<Spring>,
    blocks: &Vec<usize>,
) -> usize {
    fn possibilities_inner(
        cache: &mut FxHashMap<(Vec<Spring>, Vec<usize>), usize>,
        springs: &Vec<Spring>,
        blocks: &Vec<usize>,
    ) -> usize {
        let mut count = 0;
        if blocks.is_empty() {
            return !springs.iter().contains(&Spring::Dmgd) as usize;
        }
        if blocks[0] <= springs.len()
            && springs[0..blocks[0]].iter().all(|s| *s != Spring::Oper)
            && (springs.len() == blocks[0] || springs[blocks[0]] != Spring::Dmgd)
        {
            let remainder = if blocks[0] + 1 < springs.len() {
                &springs[blocks[0] + 1..]
            } else {
                &[]
            };
            count += possibilities(cache, &remainder.to_vec(), &blocks[1..].to_vec());
        }
        if !springs.is_empty() && springs[0] != Spring::Dmgd {
            count += possibilities(cache, &springs[1..].to_vec(), blocks);
        }
        count
    }
    let springs_vec = springs.to_vec();
    let blocks_vec = blocks.to_vec();
    if let Some(r) = cache.get(&(springs_vec, blocks_vec)) {
        *r
    } else {
        let r = possibilities_inner(cache, springs, blocks);
        cache.insert((springs.clone(), blocks.clone()), r);
        r
    }
}

fn main() {
    let mut file = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());
    let mut line = String::new();
    let (mut part1, mut part2) = (0, 0);
    while file.read_line(&mut line).unwrap() != 0 {
        let mut cache = FxHashMap::default();
        let (springs_str, numbers_str) = line[..line.len() - 1].split_once(' ').unwrap();
        let springs: &[Spring] = unsafe { std::mem::transmute(springs_str) };
        let numbers = numbers_str
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect::<Vec<usize>>();
        part1 += possibilities(&mut cache, &springs.to_vec(), &numbers);
        let long_springs_str = "?".join(repeat_n(springs_str, 5));
        let long_springs: &[Spring] = unsafe { std::mem::transmute(long_springs_str.as_bytes()) };
        let long_numbers = numbers
            .iter()
            .chain(numbers.iter())
            .chain(numbers.iter())
            .chain(numbers.iter())
            .chain(numbers.iter())
            .copied()
            .collect::<Vec<usize>>();
        part2 += possibilities(&mut cache, &long_springs.to_vec(), &long_numbers);
        line.clear();
    }
    println!("{}\n{}", part1, part2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_possibilities() {
        assert_eq!(
            possibilities(
                &mut FxHashMap::default(),
                &unsafe { std::mem::transmute::<&[u8], &[Spring]>("??? ###".as_bytes()) }.to_vec(),
                &vec![1, 1, 3],
            ),
            1,
        );
        assert_eq!(
            possibilities(
                &mut FxHashMap::default(),
                &unsafe { std::mem::transmute::<&[u8], &[Spring]>(".??..??...?##.".as_bytes()) }
                    .to_vec(),
                &vec![1, 1, 3],
            ),
            4,
        );
        assert_eq!(
            possibilities(
                &mut FxHashMap::default(),
                &unsafe { std::mem::transmute::<&[u8], &[Spring]>("?#?#?#?#?#?#?#?".as_bytes()) }
                    .to_vec(),
                &vec![1, 3, 1, 6],
            ),
            1,
        );
        assert_eq!(
            possibilities(
                &mut FxHashMap::default(),
                &unsafe { std::mem::transmute::<&[u8], &[Spring]>("????.#...#....".as_bytes()) }
                    .to_vec(),
                &vec![4, 1, 1],
            ),
            1,
        );
        assert_eq!(
            possibilities(
                &mut FxHashMap::default(),
                &unsafe {
                    std::mem::transmute::<&[u8], &[Spring]>("????.######..#####.".as_bytes())
                }
                .to_vec(),
                &vec![1, 6, 5],
            ),
            4,
        );
        assert_eq!(
            possibilities(
                &mut FxHashMap::default(),
                &unsafe { std::mem::transmute::<&[u8], &[Spring]>("?###????????".as_bytes()) }
                    .to_vec(),
                &vec![3, 2, 1],
            ),
            10,
        );
    }
}
