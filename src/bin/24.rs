use itertools::Itertools;
use num::rational::Ratio;
use num::{Signed, Zero};
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn function_params_2d(
    position: &(Ratio<i128>, Ratio<i128>, Ratio<i128>),
    velocity: &(Ratio<i128>, Ratio<i128>, Ratio<i128>),
) -> (Ratio<i128>, Ratio<i128>) {
    let coefficient = velocity.1 / velocity.0;
    let constant = coefficient * position.0 - position.1;
    (coefficient, constant)
}

fn intersection(
    first: (Ratio<i128>, Ratio<i128>),
    second: (Ratio<i128>, Ratio<i128>),
) -> Option<(Ratio<i128>, Ratio<i128>)> {
    if (second.0 - first.0).is_zero() {
        None
    } else {
        let x = (second.1 - first.1) / (second.0 - first.0);
        let y = x * first.0 - first.1;
        Some((x, y))
    }
}

fn is_after(
    initial: &(Ratio<i128>, Ratio<i128>, Ratio<i128>),
    velocity: &(Ratio<i128>, Ratio<i128>, Ratio<i128>),
    position: &(Ratio<i128>, Ratio<i128>),
) -> bool {
    (initial.0 - position.0).is_positive() ^ velocity.0.is_positive()
}

fn parse_vec(s: &str) -> (Ratio<i128>, Ratio<i128>, Ratio<i128>) {
    s.split(", ")
        .map(|s| Ratio::from_integer(s.parse::<i128>().unwrap()))
        .collect_tuple()
        .unwrap()
}

fn main() {
    let input = BufReader::new(File::open(env::args().nth(1).unwrap()).unwrap());

    let mut paths = Vec::new();
    for l in input.lines() {
        let line = l.unwrap();
        let (pos_str, vel_str) = line.split_once(" @ ").unwrap();
        let pos = parse_vec(pos_str);
        let vel = parse_vec(vel_str);
        paths.push((pos, vel));
    }
    let lower = Ratio::from_integer(200000000000000);
    let upper = Ratio::from_integer(400000000000000);
    let mut count = 0;
    for ((p1, v1), (p2, v2)) in paths.iter().tuple_combinations() {
        let f1 = function_params_2d(p1, v1);
        let f2 = function_params_2d(p2, v2);
        if let Some(ix) = intersection(f1, f2) {
            if ix.0 >= lower
                && ix.0 <= upper
                && ix.1 >= lower
                && ix.1 <= upper
                && is_after(p1, v1, &ix)
                && is_after(p2, v2, &ix)
            {
                count += 1;
            }
        }
    }
    println!("{count}");
}
