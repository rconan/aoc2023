use bitvec::prelude::{bitvec, BitSlice};
use itertools::Itertools;
use memmap2::Mmap;
use petgraph::prelude::{EdgeRef, GraphMap, Undirected};
use rustc_hash::FxHashSet;
use std::cmp::max;
use std::collections::VecDeque;
use std::env;
use std::fs::File;
use tailcall::trampoline;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    const ALL: [Self; 4] = [Self::Up, Self::Down, Self::Left, Self::Right];

    fn opposite(&self) -> Direction {
        match self {
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
        }
    }
}

type GridPath<'a> = (&'a Grid<'a>, usize, usize, Direction);

struct Grid<'a> {
    data: &'a [u8],
    row_length: usize,
    width: usize,
    height: usize,
}

impl<'a> Grid<'a> {
    fn from_input(input: &'a [u8]) -> Self {
        let (width, _) = input.iter().find_position(|&&b| b == b'\n').unwrap();
        let row_length = width + 1;
        Self {
            data: input,
            row_length,
            width,
            height: input.len() / row_length,
        }
    }

    fn apply(&self, start: usize, direction: Direction) -> Option<usize> {
        match direction {
            Direction::Up => {
                if start > self.row_length {
                    Some(start - self.row_length)
                } else {
                    None
                }
            }
            Direction::Down => {
                if start + self.row_length < self.data.len() {
                    Some(start + self.row_length)
                } else {
                    None
                }
            }
            Direction::Left => {
                if start % self.row_length > 0 {
                    Some(start - 1)
                } else {
                    None
                }
            }
            Direction::Right => {
                if self.width - start % self.row_length > 1 {
                    Some(start + 1)
                } else {
                    None
                }
            }
        }
    }

    #[inline(always)]
    fn edge_inner(
        (grid, acc, start, direction): GridPath,
    ) -> trampoline::Next<GridPath, (usize, usize, Direction)> {
        if start / grid.row_length == grid.height - 1 {
            return trampoline::Finish((start, acc, direction));
        }
        let from = direction.opposite();
        let options = Direction::ALL
            .iter()
            .filter(|&&d| {
                d != from && {
                    if let Some(next) = grid.apply(start, d) {
                        grid.data[next] != b'#'
                    } else {
                        false
                    }
                }
            })
            .collect::<Vec<_>>();
        if options.len() > 1 {
            return trampoline::Finish((start, acc, direction));
        }
        trampoline::Recurse((
            grid,
            acc + 1,
            grid.apply(start, *options[0]).unwrap(),
            *options[0],
        ))
    }

    fn edge(&self, start: usize, direction: Direction) -> Option<(usize, usize, Direction)> {
        if let Some(next) = self.apply(start, direction) {
            if self.data[next] == b'#' {
                None
            } else {
                Some(trampoline::run(
                    Self::edge_inner,
                    (self, 1, next, direction),
                ))
            }
        } else {
            None
        }
    }

    fn to_graph(&self) -> (GraphMap<usize, usize, Undirected>, usize) {
        let mut queue = VecDeque::new();
        let (start, _) = self.data.iter().find_position(|&&b| b == b'.').unwrap();
        queue.push_back((start, Direction::Down));
        let mut edges = Vec::new();
        let mut visited = FxHashSet::default();
        while let Some((from, dir)) = queue.pop_front() {
            visited.insert(from);
            if let Some((to, weight, dir)) = self.edge(from, dir) {
                edges.push((from, to, weight));
                if visited.insert(to) && to / self.row_length != self.height {
                    Direction::ALL
                        .iter()
                        .filter(|&&d| d != dir.opposite())
                        .for_each(|&d| queue.push_back((to, d)))
                }
            }
        }
        (GraphMap::from_edges(edges), start)
    }
}

fn longest_hike(input: &[u8], width: usize, start: usize) -> usize {
    let row_length = width + 1;
    fn longest_hike_inner(
        input: &[u8],
        width: usize,
        start: usize,
        row_length: usize,
        acc: usize,
        visited: &BitSlice,
        route: &[(usize, usize)],
    ) -> usize {
        if input.len() - start < row_length {
            return acc;
        }
        let mut longest = acc;
        let mut new_visits = visited.to_bitvec();
        new_visits.set(start, true);
        let mut new_route = route.to_vec();
        new_route.push((start % row_length, start / row_length));
        match input[start] {
            b'^' => {
                debug_assert!(start > row_length);
                let up_idx = start - row_length;
                if !visited[up_idx] {
                    longest = max(
                        longest,
                        longest_hike_inner(
                            input,
                            width,
                            up_idx,
                            row_length,
                            acc + 1,
                            &new_visits,
                            &new_route,
                        ),
                    );
                }
            }
            b'v' => {
                debug_assert!(start + row_length < input.len() - 1);
                let down_idx = start + row_length;
                if !visited[down_idx] {
                    longest = max(
                        longest,
                        longest_hike_inner(
                            input,
                            width,
                            down_idx,
                            row_length,
                            acc + 1,
                            &new_visits,
                            &new_route,
                        ),
                    );
                }
            }
            b'<' => {
                debug_assert!(start % row_length > 0);
                let left_idx = start - 1;
                if !visited[left_idx] {
                    longest = max(
                        longest,
                        longest_hike_inner(
                            input,
                            width,
                            left_idx,
                            row_length,
                            acc + 1,
                            &new_visits,
                            &new_route,
                        ),
                    );
                }
            }
            b'>' => {
                debug_assert!(width - start % row_length > 1);
                let right_idx = start + 1;
                if !visited[right_idx] {
                    longest = max(
                        longest,
                        longest_hike_inner(
                            input,
                            width,
                            right_idx,
                            row_length,
                            acc + 1,
                            &new_visits,
                            &new_route,
                        ),
                    );
                }
            }
            _ => {
                if start > row_length {
                    // Check up separately because it could be the start position.
                    let up_idx = start - row_length;
                    if !visited[up_idx] && input[up_idx] != b'#' {
                        longest = max(
                            longest,
                            longest_hike_inner(
                                input,
                                width,
                                up_idx,
                                row_length,
                                acc + 1,
                                &new_visits,
                                &new_route,
                            ),
                        );
                    }
                }
                for next in [start + row_length, start - 1, start + 1] {
                    // Assume all the others are fine
                    if !visited[next] && input[next] != b'#' {
                        longest = max(
                            longest,
                            longest_hike_inner(
                                input,
                                width,
                                next,
                                row_length,
                                acc + 1,
                                &new_visits,
                                &new_route,
                            ),
                        );
                    }
                }
            }
        }
        longest
    }
    longest_hike_inner(
        input,
        width,
        start,
        row_length,
        0,
        &bitvec![0; input.len()],
        &Vec::new(),
    )
}

fn longest_path(graph: &GraphMap<usize, usize, Undirected>, start: usize, end: usize) -> usize {
    fn longest_path_inner(
        acc: usize,
        graph: &GraphMap<usize, usize, Undirected>,
        at: usize,
        end: usize,
        visited: &FxHashSet<usize>,
    ) -> Option<usize> {
        if at == end {
            return Some(acc);
        }
        let mut new_visited = visited.clone();
        new_visited.insert(at);
        graph
            .edges(at)
            .filter(|e| !visited.contains(&e.target()))
            .filter_map(|e| {
                longest_path_inner(acc + e.weight(), graph, e.target(), end, &new_visited)
            })
            .max()
    }
    longest_path_inner(0, graph, start, end, &FxHashSet::default()).unwrap()
}

fn find_end(input: &[u8]) -> usize {
    let (e, _) = input.iter().rev().find_position(|&&b| b == b'.').unwrap();
    input.len() - e - 1
}

fn main() {
    let input = unsafe { Mmap::map(&File::open(env::args().nth(1).unwrap()).unwrap()).unwrap() };
    let (width, _) = input.iter().find_position(|&&b| b == b'\n').unwrap();

    let (start, _) = input[0..width]
        .iter()
        .find_position(|&&b| b == b'.')
        .unwrap();
    let end = find_end(&input);
    println!("{}", longest_hike(&input, width, start),);
    let grid = Grid::from_input(&input);
    let (graph, start) = grid.to_graph();
    println!("{}", longest_path(&graph, start, end));
}
